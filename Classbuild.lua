local Pkgsrv = require(_G.Pkgsrv)
local print1, print2, print3 = unpack(require(Pkgsrv.DebugPrint))
local MetaTableTools = require(Pkgsrv.MetaTableTools)
local FenvTools = require(Pkgsrv.FenvTools)
local BaseTypes = unpack(require(Pkgsrv.BaseTypes))

local dummyInheritance = {
    Static = {
        Public = setmetatable({}, {
            __index = getfenv(),
            __newindex = function(self, index, value) error("Unknown field" .. index) end
        }),
        Protected = setmetatable({}, {
            __index = getfenv(),
            __newindex = function(self, index, value) error("Unknown field" .. index) end
        })
    },
    Fields = {
        Public = {},
        Protected = {}
    },
    Members = {
        Public = {},
        Protected = {},
    },
    Constructor = {func = function(...) end}
}

local inheritance = {

}

local function Classbuild(definition)
    print1("Scanning class definition " .. definition.__type)

    local members = {
        Public = {},
        Protected = {},
        Private = {}
    }

    local inheritsFrom

    if (definition.__extends) then
        print1("Class " .. definition.__type .. " extends " .. definition.__extends.__type)
        inheritsFrom = inheritance[tostring(definition.__extends)]

        for vis, tab in pairs(inheritsFrom.Members) do
            for key, value in pairs(tab) do
                print3("Inheriting " .. key)
                members[vis][key] = value
            end
        end
    else
        print2("Class " .. definition.__type .. " does not extend any class")
        inheritsFrom = dummyInheritance
    end

    local fields = {
        Static = {
            Public = {},
            Protected = {},
            Private = {}
        },
        Public = {},
        Protected = {},
        Private = {}
    }

    setmetatable(fields.Static.Protected, {__index = fields.Static.Public})
    setmetatable(fields.Static.Private, {__index = fields.Static.Protected})

    setmetatable(fields.Public, {__index = inheritsFrom.Fields.Public})
    setmetatable(fields.Protected, {__index = MetaTableTools.branch(fields.Public, inheritsFrom.Fields.Protected)})
    setmetatable(fields.Private, {__index = fields.Protected})

    local staticClass = {
        Module = {},
        Public = {},
        Protected = {},
        Private = {},
        Self = {}
    }

    staticClass.Public.proxy = setmetatable({__type = definition.__type}, {__index = inheritsFrom.Static.Public})
    staticClass.Public.handle = setmetatable({}, {
        __index = staticClass.Public.proxy,
        __newindex = function(self, index, value)
            if (fields.Static.Public[index]) then
                staticClass.Public.proxy[index] = value
            else
                inheritsFrom.Static.Public[index] = value
            end
        end
    })

    staticClass.Protected.proxy = setmetatable({}, {__index = MetaTableTools.branch(staticClass.Public.proxy, inheritsFrom.Static.Protected)})
    staticClass.Protected.handle = setmetatable({}, {
        __index = staticClass.Protected.proxy,
        __newindex = function(self, index, value)
            if (fields.Static.Protected[index]) then
                if (rawget(fields.Static.Protected, index)) then
                    staticClass.Protected.proxy[index] = value
                else
                    staticClass.Public.handle[index] = value
                end
            else
                inheritsFrom.Static.Protected[index] = value
            end
        end
    })

    staticClass.Private.proxy = setmetatable({}, {__index = staticClass.Protected.proxy})
    staticClass.Private.handle = setmetatable({}, {
        __index = staticClass.Private.proxy,
        __newindex = function(self, index, value)
            if (fields.Static.Private[index]) then
                if (rawget(fields.Static.Private, index)) then
                    staticClass.Private.proxy[index] = value
                else
                    staticClass.Protected.handle[index] = value
                end
            end
        end
    })

    staticClass.Module.proxy = setmetatable({__type = "Type", type = definition.__type}, {
        __index = staticClass.Public.proxy,
    })
    staticClass.Module.handle = setmetatable({}, {
        __index = staticClass.Module.proxy,
        __newindex = function(self, index, value)
            staticClass.Public.handle[index] = value
        end,
        __call = BaseTypes.makeParameter
    })

    for i, value in ipairs(definition) do
        if (_G.type(value) == "Function" and value.scope == "Static" and value.visibility == "Public") then
            staticClass.Constructor = FenvTools.FunctionWrapper(
                value.func,
                staticClass.Private.handle,
                {[definition.__type] = staticClass.Private.handle}
            )
            definition[i] = nil
        end
    end

    print3()
    for key, value in pairs(definition) do
        print2(key .. " -> " .. tostring(value))
        print3("Checking if " .. key .. " has a type that we need: " .. _G.type(value))

        if (value.__type and key ~= "__extends") then
            if (_G.type(value) == "Field") then
                if (fields.Private[key]) then
                    error("Attempt to redeclare field " .. key)
                end
    
                if (value.scope == "Static") then
                    print3("Setting fields.Static." .. value.visibility .. "." .. key .. " = true")
                    fields.Static[value.visibility][key] = true
                else
                    print3("Setting fields." .. value.visibility .. "." .. key .. " = true")
                    fields[value.visibility][key] = true
                end
            else
                if (value.scope == "Static") then
                    if (_G.type(value) == "Function") then
                        local super = nil

                        if (staticClass.Protected.handle[key] and _G.type(staticClass.Protected.handle[key]) == "function") then
                            super = staticClass.Protected.handle[key]
                        end

                        staticClass[value.visibility].proxy[key] = FenvTools.FunctionWrapper(
                            value.func,
                            staticClass.Private.handle,
                            {[definition.__type] = staticClass.Private.handle, super = super}
                        )
                    elseif (_G.type(value) == "EventPrototype") then
                        staticClass[value.visibility].proxy[key] = Event.new(value.params)
                    else
                        print3("Setting staticClass." ..value.visibility .. ".proxy" .. key .. " = " ..tostring(value))
                        staticClass[value.visibility].proxy[key] = value
                    end
                else
                    if (_G.type(value) == "Function") then
                        if (key == definition.__type) then
                            value.super = inheritsFrom.Constructor.func
                        else
                            if (members.Protected[key]) then
                                value.super = members.Protected[key].func
                            elseif (members.Public[key]) then
                                value.super = members.Public[key].func
                            end
                        end
                    end
                    print3("Setting members." .. value.visibility .. "." .. key .. " = " ..tostring(value))
                    members[value.visibility][key] = value
                end
            end
        end
        

        print3()
    end

    if staticClass.Constructor then staticClass.Constructor {} end

    function staticClass.Module.proxy.new(args)
        local instance = {
            Public = {},
            Private = {}
        }

        instance.Public.proxy = setmetatable({__type = definition.__type}, {__index = staticClass.Public.proxy})
        instance.Public.handle = setmetatable({}, {
            __index = instance.Public.proxy,
            __newindex = function(self, index, value)
                if (fields.Public[index]) then
                    instance.Public.proxy[index] = value
                else
                    staticClass.Public.handle[index] = value
                end
            end
        })

        instance.Private.proxy = setmetatable({}, {__index = MetaTableTools.branch(instance.Public.proxy, staticClass.Private.proxy)})
        instance.Private.handle = setmetatable({}, {
            __index = MetaTableTools.branch(instance.Private.proxy),
            __newindex = function(self, index, value)
                if (rawget(fields.Protected, index)) then
                    instance.Private.proxy[index] = value
                elseif (fields.Private[index]) then
                    if (rawget(fields.Private, index)) then
                        instance.Private.proxy[index] = value
                    else
                        instance.Public.handle[index] = value
                    end
                else
                    staticClass.Private.handle[index] = value
                end
            end
        })

        instance.Protected = instance.Private

        for visibility, tab in pairs(members) do
            for key, value in pairs(tab) do
                if (_G.type(value) == "Function") then
                    instance[visibility].proxy[key] = FenvTools.FunctionWrapper(
                        value.func,
                        instance.Private.handle,
                        {[definition.__type] = staticClass.Private.handle, self = instance.Private.handle, super = value.super}
                    )
                elseif (_G.type(value) == "EventPrototype") then
                    instance[visibility].proxy[key] = Event.new(value.params)
                end
            end
        end

        if (rawget(instance.Public.proxy, definition.__type)) then
            instance.Public.proxy[definition.__type](args)
        end

        instance.Public.proxy[definition.__type] = nil

        return instance.Public.handle
    end

    local me = {
        Static = {
            Public = staticClass.Public.handle,
            Protected = staticClass.Protected.handle
        },
        Fields = {
            Public = fields.Public,
            Protected = fields.Protected
        },
        Members = {
            Public = members.Public,
            Protected = fields.Protected
        },
        Constructor = members.Public[definition.__type],
    }

    inheritance[tostring(staticClass.Module.handle)] = me

    return staticClass.Module.handle
end

return Classbuild