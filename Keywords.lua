-- local Keywords, Public, Private, Protected, Static = unpack(require("Keywords"))
local Pkgsrv = require(_G.Pkgsrv)
local MetaTableTools = require(Pkgsrv.MetaTableTools)

local Keywords_P = {}
local Keywords_MT = {__index = Keywords_P, __newindex = MetaTableTools.readonly("Cannot write to read-only module Keywords")}

local function makeKeyword(key, value)
    return function(input)
        -- special case for a Type (turn it into a field instance)
        local data = input[1]

        if (_G.type(data) == "Type") then
            data = {__type = "Field", type = data.type}
        elseif (_G.type(data) == "function") then
            data = {__type = "Function", func = data}
        end

        data[key] = value

        return data
    end
end

Keywords_P.Public = makeKeyword("visibility", "Public")
Keywords_P.Private = makeKeyword("visibility", "Private")
Keywords_P.Protected = makeKeyword("visibility", "Protected")
Keywords_P.Static = makeKeyword("scope", "Static")

return {setmetatable({}, Keywords_MT), Keywords_P.Public, Keywords_P.Private, Keywords_P.Protected, Keywords_P.Static}