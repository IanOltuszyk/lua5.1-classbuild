local Pkgsrv = require(_G.Pkgsrv)
local Connection = require(Pkgsrv.Connection)

local Event_P = {}
local Event_MT = {
    __index = Event_P,
    __call = function(self, args)
        -- TODO: check form of args
        return {__type = "EventPrototype", params = args}
    end
}

function Event_P.new(args)
    local instance = {}
    local instance_P = {__type = "Event"}
    local instance_MT = {__index = instance_P}
    local connections = {}

    function instance_P:Fire(fireArgs)
        -- TODO: check types
        for key, connection in pairs(connections) do
            connection(fireArgs)
        end
    end

    function instance_P:Connect(func)
        if (not connections[tostring(func)]) then
            connections[tostring(func)] = func
            return Connection.new(instance, func)
        end
    end

    function instance_P:Disconnect(func)
        connections[tostring(func)] = nil
    end

    return setmetatable(instance, instance_MT)
end

return setmetatable({}, Event_MT)