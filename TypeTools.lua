local Pkgsrv = require(_G.Pkgsrv)
local BaseTypes = require(Pkgsrv.BaseTypes)
local MetaTableTools = require(Pkgsrv.MetaTableTools)

local TypeTools_P = {}
local TypeTools_MT = {__index = TypeTools_P, __newindex = MetaTableTools.readonly("Cannot write to read-only module TypeTools")}

function TypeTools_P.AssertType(obj, type)
    if (type.Optional) then
        return
    end

    assert(obj, "Parameter " .. type.name .. " missing")

    if (type.type == "any") then
        return
    end

    local objType = _G.type(obj)

    if (_G.type(type.type) == "table") then
        local flag = false
        for _, v in ipairs(type.type) do
            if (objType == v or v == "any") then
                flag = true
                break
            end
        end
        assert(flag, "Parameter " .. type.name .. " incorrect type")
        return
    end

    assert(objType == type.type, "Parameter " .. type.name .. " incorrect type")
end

return setmetatable({}, TypeTools_MT)