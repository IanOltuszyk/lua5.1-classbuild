local Pkgsrv = require(_G.Pkgsrv)
local TypeTools = require(Pkgsrv.TypeTools)
local FenvTools = require(Pkgsrv.FenvTools)

local function Function(input)
    local params = {}
    for i = 1, #input - 1 do
        params[i] = input[i]
    end
    local func = input[#input]

    return function(args)
        for _, v in ipairs(params) do
            TypeTools.AssertType(args[v.name], v)
        end

        local wrapped = FenvTools.FunctionWrapper(func, getfenv(), args)

        return wrapped(args)


    end
end

return Function