local module = {}
local module_MT_preinit = {
    __index = function(t, k)
        error("Pkgsrv must be initialized using Pkgsrv.init(func : function, rootManifest : any) prior to use")
    end
}
module.init = function (func, manifest)
    module.init = nil
    local loaded = func(manifest)
    setmetatable(module, {
        __index = function(self, index)
            return loaded[index]
        end
    })
end
setmetatable(module, module_MT_preinit)

return module