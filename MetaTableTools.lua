local MetaTableTools_P = {}
local MetaTableTools_MT = {__index = MetaTableTools_P}

function MetaTableTools_P.readonly(message)
    return function(...) error(message) end
end

function MetaTableTools_P.branch(...)
    local args = {...}

    if (#args == 0) then
        return nil
    end

    if (#args == 1) then
        return args[1]
    end

    return function(_, index)
        for i = 1, #args do
            if args[i] then
                local r = args[i][index]
                if (r) then return r end
            end
        end
    end
end

MetaTableTools_MT.__newindex = MetaTableTools_P.readonly("Cannot write to read-only module MetaTableTools")

return setmetatable({}, MetaTableTools_MT)