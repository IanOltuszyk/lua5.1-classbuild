local Pkgsrv = require(_G.Pkgsrv)
local TypeTools = require(Pkgsrv.TypeTools)
local FenvTools = require(Pkgsrv.FenvTools)

local Connection_P = {}
local Connection_MT = {
    __index = Connection_P
}

function Connection_P.new(parent, func)
    local instance_P = {__type = "Connection"}
    local instance_MT = {__index = instance_P}
    
    function instance_P:Disconnect()
        parent:Disconnect(func)
    end

    return setmetatable({}, instance_MT)
end

return setmetatable({}, Connection_MT)