local Pkgsrv = require(_G.Pkgsrv)
local MetaTableTools = require(Pkgsrv.MetaTableTools)

local FenvTools_P = {}
local FenvTools_MT = {__index = FenvTools_P, __newindex = MetaTableTools.readonly("Cannot write to read-only module FenvTools")}

function FenvTools_P.FunctionWrapper(func, fenv, inject)
    return function(...)
        local oldFenv = getfenv(func)
        local newFenv = setmetatable(inject, {__index = fenv})

        setfenv(func, newFenv)
        local result = func(...)
        setfenv(func, oldFenv)

        return result
    end
end

return setmetatable({}, FenvTools_MT)