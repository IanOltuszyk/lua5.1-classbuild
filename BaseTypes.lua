-- local BaseTypes, Boolean, Number, String, UserData, Table, Optional, Any = unpack(require("BaseTypes"))
local Pkgsrv = require(_G.Pkgsrv)
local MetaTableTools = require(Pkgsrv.MetaTableTools)

local BaseTypes_P = {}
local BaseTypes_MT = {__index = BaseTypes_P, __newindex = MetaTableTools.readonly("Cannot write to read-only module BaseTypes")}

function BaseTypes_P.makeParameter(self, parameterName)
    local instance = {__type = "Parameter", type = self.type, name = parameterName}
    return instance
end

local function makeType(typeName, default)
    local type_P = {__type = "Type", type = typeName, default = default}
    local type_MT = {
        __index = type_P,
        __newindex = MetaTableTools.readonly("Cannot write to read-only type"),
        __call = BaseTypes_P.makeParameter
    }
    return setmetatable({}, type_MT)
end

BaseTypes_P.Boolean = makeType("boolean", false)
BaseTypes_P.Number = makeType("number", 0)
BaseTypes_P.String = makeType("string", "")
BaseTypes_P.UserData = makeType("userdata")
BaseTypes_P.Function = makeType("function")
BaseTypes_P.Table = makeType("table", {})
BaseTypes_P.Any = makeType("any")

local oldTypeFunc = type
function _G.type(obj)
    local oldType = oldTypeFunc(obj)
    if (oldType == "table" and obj.__type) then
        return obj.__type
    end

    return oldType
end

function BaseTypes_P.Optional(input)
    input[1].Optional = true
end

function BaseTypes_P.Union(input)
    local types = {}

    for k, v in pairs(input) do
        types[#types+1] = v("").type
    end

    return makeType(types)
end

return {setmetatable({}, BaseTypes_MT), BaseTypes_P.Boolean, BaseTypes_P.Number, BaseTypes_P.String, BaseTypes_P.UserData, BaseTypes_P.Table, BaseTypes_P.Optional, BaseTypes_P.Union, BaseTypes_P.Any}